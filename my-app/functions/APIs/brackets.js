

const { db } = require('../util/admin');

exports.getAllTodos = (request, response) => {
	db
		.collection('brackets')
		.orderBy('createdAt', 'desc')
		.get()
		.then(data => {
			let brackets = [];
			data.forEach(doc => {
				brackets.push({
                    todoId: doc.id,
                    title: doc.data().title,
					body: doc.data().body,
					createdAt: doc.data().createdAt,
				});
			});
			return response.json(brackets);
		})
		.catch((err) => {
			console.error(err);
			return response.status(500).json({ error: err.code});
		});
};