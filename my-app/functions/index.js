//index.js

const functions = require('firebase-functions');
const app = require('express')();

const {
    getAllTodos,
    postOneTodo
} = require('./APIs/brackets')




app.post('/brackets', postOneTodo);
app.get('/brackets', getAllTodos);
exports.api = functions.https.onRequest(app);