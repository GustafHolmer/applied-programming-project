import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Frontend for tournament app
        </p>
      </header>
    </div>
  );
}

export default App;
